package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    String sign, value1, value2;
    double num1, num2, result;
    TextView input, signBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (TextView)findViewById(R.id.textView);
        signBox = (TextView)findViewById(R.id.textView2);
    }
    @SuppressLint("SetTextI18n")
    public void onButtonClickOne(View v){
        input.setText(input.getText() + "1");
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClickTwo(View v){
        input.setText(input.getText() + "2");
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClickThree(View v){
        input.setText(input.getText() + "3");
    }

    @SuppressLint("SetTextI18n")
    e{
        input.setText(input.getText() + "4");
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClickFive(View v){
        input.setText(input.getText() + "5");
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClickSix(View v){
        input.setText(input.getText() + "6");
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClickSeven(View v){
        input.setText(input.getText() + "7");
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClickEight(View v){
        input.setText(input.getText() + "8");
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClickNine(View v){
        input.setText(input.getText() + "9");
    }

    @SuppressLint("SetTextI18n")
    public void onButtonClickZero(View v){
        input.setText(input.getText() + "0");
    }

    public void btnClick_add(View view) {
        sign = "+";
        value1 = input.getText().toString();
        input.setText(null);
        signBox.setText("+");
    }

    public void btnClick_subtract(View view) {
        sign = "-";
        value1 = input.getText().toString();
        input.setText(null);
        signBox.setText("-");
    }

    public void btnClick_multiply(View view) {
        sign = "*";
        value1 = input.getText().toString();
        input.setText(null);
        signBox.setText("×");
    }

    public void btnClick_divide(View view) {
        sign = "/";
        value1 = input.getText().toString();
        input.setText(null);
        signBox.setText("÷");
    }

    @SuppressLint("SetTextI18n")
    public void btnClick_equal(View view) {
        if (sign == null) {
            signBox.setText("Error!");
        } else if (input.getText().equals("")) {
            signBox.setText("Error!");
        } else if ((sign.equals("+") || sign.equals("-") || sign.equals("*") || sign.equals("/")) && value1.equals("")) {
            signBox.setText("Error!");
        } else {
            switch (sign) {
                default:
                    break;
                case "+":
                    value2 = input.getText().toString();
                    num1 = Double.parseDouble(value1);
                    num2 = Double.parseDouble(value2);
                    result = num1 + num2;
                    input.setText(result + "");
                    sign = null;
                    signBox.setText(null);
                    break;
                case "-":
                    value2 = input.getText().toString();
                    num1 = Double.parseDouble(value1);
                    num2 = Double.parseDouble(value2);
                    result = num1 - num2;
                    input.setText(result + "");
                    sign = null;
                    signBox.setText(null);
                    break;
                case "*":
                    value2 = input.getText().toString();
                    num1 = Double.parseDouble(value1);
                    num2 = Double.parseDouble(value2);
                    result = num1 * num2;
                    input.setText(result + "");
                    sign = null;
                    signBox.setText(null);
                    break;
                case "/":
                    value2 = input.getText().toString();
                    num1 = Double.parseDouble(value1);
                    num2 = Double.parseDouble(value2);
                    if(num2 != 0) {
                        result = num1 / num2;
                        input.setText(result + "");
                        sign = null;
                        signBox.setText(null);
                    } else {
                        input.setText("Error");
                    }
                    break;
            }

        }
    }
    public void btnClick_clear(View view) {

        input.setText(null);
        signBox.setText(null);
        value1 = null;
        value2 = null;
        sign = null;
    }
}